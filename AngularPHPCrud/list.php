<?php
require 'connect.php';
error_reporting(E_ERROR);
$customers = [];
$sql = "SELECT * FROM customers";

if($result = mysqli_query($con,$sql)) 
{
	$cr = 0;
	while ($row = mysqli_fetch_assoc($result)) 
	{
		$customers[$cr]['cId'] = $row['cId'];
		$customers[$cr]['fName'] = $row['fName'];
		$customers[$cr]['lName'] = $row['lName'];
		$customers[$cr]['email'] = $row['email'];
		$cr++;
	}

//print_r($customers);
	echo json_encode($customers);
}
else
{
	http_response_code(404);
}
?>