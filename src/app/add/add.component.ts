import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomersService} from '../customers.service';
import {Router} from '@angular/router';
import {Customers} from '../customers';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private _customersService: CustomersService,
    private router: Router ) { }

  addForm: FormGroup;
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      fName: ['', Validators.required],
      lName: ['', [Validators.required, Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.maxLength(50)]],
    });
  }

  onSubmit() {
    this._customersService.createCustomers(this.addForm.value)
    .subscribe(data => {
      this.router.navigate(['view']);
    });
  }

}
 