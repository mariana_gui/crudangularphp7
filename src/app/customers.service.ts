import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Customers } from './customers';
@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private http: HttpClient) { }

  getCustomers() { 
    return this.http.get<Customers[]>('http://localhost/angularphpcrud/list.php');
  }

  deleteCustomers (id:number) {
    return this.http.delete<Customers[]>('http://localhost/angularphpcrud/delete.php?id=' + id);
  }

  createCustomers(customers: Customers) {
    return this.http.post('http://localhost/angularphpcrud/insert.php', customers);
  }

  getById(id:number) {
    return this.http.get<Customers[]>('http://localhost/angularphpcrud/getById.php?id=' + id);
  }

  updateCustomer(customers: Customers) {
    return this.http.put('http://localhost/angularphpcrud/update.php' + '?id=' + customers.cId, customers)
  }


}
