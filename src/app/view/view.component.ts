import { Component, OnInit } from '@angular/core';
import {Customers} from '../customers';
import { CustomersService } from '../customers.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  customers: Customers[];
  _id: any;
  constructor(private _customersService: CustomersService,
    private router: Router) { }

  ngOnInit() {
    this._customersService.getCustomers()
    .subscribe((data: Customers[]) => {
      this.customers = data;
      console.log(this.customers);
    });
  }

  delete(customers: Customers): void {
    this._customersService.deleteCustomers(customers.cId)
    .subscribe(data => {
      this.customers = this.customers.filter(u => u !== customers);
    });
  }

  edit(customers: Customers){
    this._id = customers.cId;
    this.router.navigate(['edit/' + this._id])
  }
}
