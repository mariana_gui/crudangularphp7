import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomersService} from '../customers.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Customers} from '../customers';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private _customersService: CustomersService,
    private router: Router,
    private routes: ActivatedRoute ) { }

  addForm: FormGroup
  ngOnInit() {
    const routeParams = this.routes.snapshot.params;

    this.addForm = this.formBuilder.group({
      cId: [''],
      fName: ['', Validators.required],
      lName: ['', [Validators.required, Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.maxLength(50)]],
    });
    
    this._customersService.getById(routeParams.id).subscribe((data: any) => {
      this.addForm.patchValue(data);
  });
  }

  update() {
    this._customersService.updateCustomer(this.addForm.value).subscribe(() => {
      this.router.navigate(['view']);
    },
    error => {
      alert(error);
    })

  }
  }

